rm(list = ls())
packs <- c("caret")
lapply(packs, library, character.only = TRUE)

# ---- Load twitter data ----

load("data_clean/dfTwitter.Rda")
mDTM = as.matrix(dfTwitter[,c(2:58008)])

# ---- Female ----

load("result/cv_female.RData")

pdf("writeup/figure/cv_female.pdf")
plot(cv_female)
dev.off()
# Predictive performance
pred_female <- predict(cv_female, newdata = as.matrix(mDTM)[-inTrain, ])
confusionMatrix(data = pred_female, vYFemale[-inTrain])

# ---- Democrat ----
load("result/cv_democrat.RData")

plot(cv_democrat)
pred_democrat <- predict(cv_democrat, newdata = as.matrix(mDTM)[-inTrain, ])
confusionMatrix(data = pred_democrat, vYDemocrat[-inTrain])