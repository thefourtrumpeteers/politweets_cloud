#Information retrieval at party level, not congressman level

require(tm)

#the commented code creates the data. we now just load the final data to save time, 
#but the code is here for reference

# data = read.csv('df_twitter.csv')
# dtmTweets = data.matrix(data[,48:57980])
# partyHandle = data[,c('user.screen_name','party','gender.y','age.y','region.y','percentUrbanPop')]
# #classify bottom quartile as rural
# urban = rep('rural',dim(partyHandle)[1])
# urban[partyHandle[,'percentUrbanPop'] > 72.40] = 'urban'
# partyHandle[,'percentUrbanPop'] = urban
# names(partyHandle) = c('user.screen_name','party','gender','age','region','urban');
# rm(data)
# rm(urban)

#load data
load('data_clean/infRet.RData')
#media articles
news = read.csv('data/News.csv')

#exploratory data analysis
barplot(table(partyHandle$party))
barplot(table(partyHandle$gender))
hist(partyHandle$age)
barplot(table(partyHandle$region))

#removed from dataset, but see above code for how to get this variable
#hist(partyHandle$percentUrbanPop)
#summary(partyHandle$percentUrbanPop)

#put age into groups
age = rep("<= 45",dim(partyHandle)[1])
age[partyHandle$age > 45] = "46 - 60"
age[partyHandle$age > 60] = "61 - 75"
age[partyHandle$age > 75] = "> 75"
partyHandle$age = age
rm(age)

##DEFINE FUNCTIONS##

#function to clean text vector
#inputs: text
#output: cleaned text
cleanText = function(text){
  #convert to string
  char = as.character(text)
  #lowercase
  lower = tolower(char)
  #remove \n 's
  removeN =  gsub("\n", " ", lower)
  #remove punctuation
  noPunc = gsub("[[:punct:]]", "", removeN)
  #put into word vector
  textV = unlist(strsplit(noPunc, " ",fixed = TRUE))
  #remove empty words
  return(textV[which(textV!="")])
}

#Comptuer the distance between last row of matrix and every other row.
#inputs: matrix
#output: vector of distances
lastDist = function(matrix) {
  d = dim(matrix)[1]
  out = NULL
  #loop through rows and record distance between row i and last row
  for (i in 1:d) {
    mat = matrix[c(i,d),]
    out[i] = dist(mat)
  }
  return(out)
}

#function to create document term matrix
#input: list of documents
#output: Document term matrix
createDTM = function(text){
  #create corpus
  corp = Corpus(VectorSource(text))
  
  #convert documents to lowercase
  corp = tm_map(corp, content_transformer(tolower))
  #remove punctuation in documents
  corp = tm_map(corp, removePunctuation)
  #remove numbers in docuemnts
  corp = tm_map(corp, removeNumbers)
  
  #create the document term matrix for the documents in corp
  dtm = as.matrix(DocumentTermMatrix(corp))
  return(dtm)
}

#function to create distance between query and congressmen
#inputs: query, DTM of congressman tweets
#outputs: DTM with distance of each document to query in last column
myTextMiner = function(query, dtm){
  #create the document term matrix for the documents in corp
  termMat = as.matrix(dtm)
  #get list of words
  words = colnames(termMat)
  #if words of query are not tweeted, get rid of them
  query2 = query[query %in% words]
  
  #for idf weighting
  D=dim(termMat)[1]
  
  #add row for search query. first append row of zeros.
  #the dim(termMat)[2] specifies the length of the appended vector to be equal to
  #the length of the of matrix
  termMat = rbind(termMat,rep(0,dim(termMat)[2]))
  #replace row of zeros with counts of words in search query.
  #table counts each word. unique(words) specifies which columns to enter the word counts
  termMat[dim(termMat)[1],names(table(query2))] = as.vector(table(query2))
  
  #generate length of each document, which corresponds to the sum of each row.
  docLength = rowSums(termMat)
  #Normalize word counts by document length
  termMatLen = termMat/docLength
  
  #idf weighting
  n = colSums(termMatLen)
  idf = log(D/n)
  termMatNorm = t(t(termMatLen) * idf)
  
  #dist outputs a matrix of the euclidean distance between each row.
  #the last row contains the distance between each documents and the query
  distanceMetric = lastDist(termMatNorm)
  #append the distances to the normalized matrix
  termMatNorm = cbind(termMatNorm,distanceMetric)
  
  #output columns of matrix corresponding to search query, along with the distance
  output = termMatNorm[,c(unique(query2),"distanceMetric")]
  return(output)
}

#find distance between query and each party's tweets
#inputs: 1. classification type (e.g. party, gender, etc)
#        2. string version of classification type (e.g. "party", "gender", etc)
#outputs: matrix with average rank of each factor of class and number of observations in that class
# computeRanks = function(string,class,n,distances) {
#   rankClass = partyHandle[distances[2:n],string]
#   ranks = NULL
#   count = NULL
#   for (i in 1:length(class)) {
#     rank1 = seq(1,n-1)[rankClass == class[i]]
#     rank2 = rank1[!is.na(rank1)]
#     ranks[i] = mean(rank2)
#     count[i] = sum(rankClass ==  class[i],na.rm = TRUE)
#   }
#   names(ranks) = class
#   retMat = rbind(ranks,count)
#   return(retMat)
# }

#find distance between query and each party's tweets
#inputs: 1. classification type (e.g. party, gender, etc)
#        2. string version of classification type (e.g. "party", "gender", etc)
#        3. vector of distances to query
#outputs: matrix with average rank of each factor of class and number of observations in that class
computeRanks = function(string,class,distances) {
  sumsq = NULL
  rank = NULL
  count = NULL
  for (i in 1:length(class)) {
    sumsq2 = sum(distances[partyHandle[,string] == class[i]]^2,na.rm = TRUE)
    count[i] = sum(partyHandle[,string] == class[i],na.rm = TRUE)
    sumsq[i] = sumsq2 / count[i]
  }
  ranks = rank(sumsq)
  names(sumsq) = class
  retMat = rbind(sumsq,ranks,count)
  return(retMat)
}

#find distance between query and each party's tweets
#inputs: Query, DTM of congressman tweets.
#outputs: 1. distance between query and each party's tweets
#         2. ranking of distances
mediaDistance = function(query,DTM,class){
  #clean text and stem
  article = cleanText(query)
  article = stemDocument(article)
  
  #calculate distance between article and congressmen
  distanceArticle = myTextMiner(article,DTM)
  #distances = distanceArticle[,dim(distanceArticle)[2]]
  #distances = order(distanceArticle[,dim(distanceArticle)[2]])
  distances = distanceArticle[1:(dim(distanceArticle)[1]-1),dim(distanceArticle)[2]]
  
  #return(list(distances,order(distances)[2:length(distances)]))
  #return(distances)
  
  #rank the distances and see who is democrat or republican
  n = dim(distanceArticle)[1]
  
  #political party
  if (class == "party") {
    return(computeRanks("party",party,distances))
  }
  #gender
  if (class == "gender") {
    return(computeRanks("gender",gender,distances))
  }
  if (class == "age") {
    return(computeRanks("age",age,distances))
  }
  if (class == "region") {
    return(computeRanks("region",region,distances))
  }
  #ubran/rural
  if (class == "urban") {
    return(computeRanks("urban",urban,distances))
  }
  else { break }
}

lasso = function(query,DTM,class) {
  #clean text and stem
  I = nlevels(query)
  article = NULL
  for (i in 1:I) {
    article[[i]] = cleanText(query[i])
    article[[i]] = stemDocument(article[[i]])
  }
  
  #run lasso on DTM
  if (class == "party") {
    outcome = 1*(partyHandle[,class]=="Democrat")
  }
  if (class == "gender") {
    outcome = 1*(partyHandle[,class]=="F")
  }
  if (class == "urban") {
    outcome = 1*(partyHandle[,class]=="urban")
  }
  
  #run lasso
  las=cv.glmnet(x=DTM[is.na(outcome)==FALSE,],
                y=outcome[is.na(outcome)==FALSE],
                alpha=1,standardize = FALSE)
  
  termMat = as.matrix(DTM)
  #get list of words
  words = colnames(termMat)
  #if words of query are not tweeted, get rid of them
  query2 = NULL
  for (i in 1:nlevels(query)) {
    query2[[i]] = article[[i]][article[[i]] %in% words]
  }
  
  #add row for each search query. first append rows of zeros.
  #the dim(termMat)[2] specifies the length of the appended vector to be equal to
  #the length of the of matrix
  termMat = rbind(termMat,matrix(0,nrow=I,ncol=dim(termMat)[2]))
  #replace row of zeros with counts of words in search query.
  #table counts each word. unique(words) specifies which columns to enter the word counts
  for (i in 1:I) {
    termMat[dim(termMat)[1]-7+i,names(table(query2[[i]]))] = as.vector(table(query2[[i]]))
  }
  
  #predict outcomes  
  predX = termMat[(dim(termMat)[1] - I + 1):dim(termMat)[1],]
  prediction = predict(las,newx=predX,type='response')
  return(prediction)
}

#classification types
party = c("Republican", "Democrat") #ignore independents
gender = c("M","F")
age = c("<= 45", "46 - 60", "61 - 75", "> 75")
region = c("MW","NE","SO","W")
urban = c("rural","urban")

##PERFORM ANALYSIS##

#article text
mediaDistance(news$Text[1],dtmTweets,'party')
mediaDistance(news$Text[2],dtmTweets,'party')
mediaDistance(news$Text[3],dtmTweets,'party')
mediaDistance(news$Text[4],dtmTweets,'party')
mediaDistance(news$Text[5],dtmTweets,'party')
mediaDistance(news$Text[6],dtmTweets,'party')
mediaDistance(news$Text[7],dtmTweets,'party')

mediaDistance(news$Text[1],dtmTweets,'gender')
mediaDistance(news$Text[2],dtmTweets,'gender')
mediaDistance(news$Text[3],dtmTweets,'gender')
mediaDistance(news$Text[4],dtmTweets,'gender')
mediaDistance(news$Text[5],dtmTweets,'gender')
mediaDistance(news$Text[6],dtmTweets,'gender')
mediaDistance(news$Text[7],dtmTweets,'gender')

mediaDistance(news$Text[1],dtmTweets,'age')
mediaDistance(news$Text[2],dtmTweets,'age')
mediaDistance(news$Text[3],dtmTweets,'age')
mediaDistance(news$Text[4],dtmTweets,'age')
mediaDistance(news$Text[5],dtmTweets,'age')
mediaDistance(news$Text[6],dtmTweets,'age')
mediaDistance(news$Text[7],dtmTweets,'age')

mediaDistance(news$Text[1],dtmTweets,'region')
mediaDistance(news$Text[2],dtmTweets,'region')
mediaDistance(news$Text[3],dtmTweets,'region')
mediaDistance(news$Text[4],dtmTweets,'region')
mediaDistance(news$Text[5],dtmTweets,'region')
mediaDistance(news$Text[6],dtmTweets,'region')
mediaDistance(news$Text[7],dtmTweets,'region')

mediaDistance(news$Text[1],dtmTweets,'urban')
mediaDistance(news$Text[2],dtmTweets,'urban')
mediaDistance(news$Text[3],dtmTweets,'urban')
mediaDistance(news$Text[4],dtmTweets,'urban')
mediaDistance(news$Text[5],dtmTweets,'urban')
mediaDistance(news$Text[6],dtmTweets,'urban')
mediaDistance(news$Text[7],dtmTweets,'urban')

#headlines
mediaDistance(news$Headline[1],dtmTweets,'party')
mediaDistance(news$Headline[2],dtmTweets,'party')
mediaDistance(news$Headline[3],dtmTweets,'party')
mediaDistance(news$Headline[4],dtmTweets,'party')
mediaDistance(news$Headline[5],dtmTweets,'party')
mediaDistance(news$Headline[6],dtmTweets,'party')
mediaDistance(news$Headline[7],dtmTweets,'party')

mediaDistance(news$Headline[1],dtmTweets,'gender')
mediaDistance(news$Headline[2],dtmTweets,'gender')
mediaDistance(news$Headline[3],dtmTweets,'gender')
mediaDistance(news$Headline[4],dtmTweets,'gender')
mediaDistance(news$Headline[5],dtmTweets,'gender')
mediaDistance(news$Headline[6],dtmTweets,'gender')
mediaDistance(news$Headline[7],dtmTweets,'gender')

mediaDistance(news$Headline[1],dtmTweets,'age')
mediaDistance(news$Headline[2],dtmTweets,'age')
mediaDistance(news$Headline[3],dtmTweets,'age')
mediaDistance(news$Headline[4],dtmTweets,'age')
mediaDistance(news$Headline[5],dtmTweets,'age')
mediaDistance(news$Headline[6],dtmTweets,'age')
mediaDistance(news$Headline[7],dtmTweets,'age')

mediaDistance(news$Headline[1],dtmTweets,'region')
mediaDistance(news$Headline[2],dtmTweets,'region')
mediaDistance(news$Headline[3],dtmTweets,'region')
mediaDistance(news$Headline[4],dtmTweets,'region')
mediaDistance(news$Headline[5],dtmTweets,'region')
mediaDistance(news$Headline[6],dtmTweets,'region')
mediaDistance(news$Headline[7],dtmTweets,'region')

mediaDistance(news$Headline[1],dtmTweets,'urban')
mediaDistance(news$Headline[2],dtmTweets,'urban')
mediaDistance(news$Headline[3],dtmTweets,'urban')
mediaDistance(news$Headline[4],dtmTweets,'urban')
mediaDistance(news$Headline[5],dtmTweets,'urban')
mediaDistance(news$Headline[6],dtmTweets,'urban')
mediaDistance(news$Headline[7],dtmTweets,'urban')

#lasso
lassoPartyT = lasso(news$Text,dtmTweets,'party')
lassoGenderT = lasso(news$Text,dtmTweets,'gender')
lassoUrbanT = lasso(news$Text,dtmTweets,'urban')
lassoPartyH = lasso(news$Headline,dtmTweets,'party')
lassoGenderH = lasso(news$Headline,dtmTweets,'gender')
lassoUrbanH = lasso(news$Headline,dtmTweets,'urban')
#commbine into table
lassoRes = cbind(lassoPartyT, lassoPartyH,
                 lassoGenderT, lassoUrbanH,
                 lassoUrbanT, lassoGenderH)