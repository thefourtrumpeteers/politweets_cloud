import tweepy
import pandas as pd
import time, datetime
import os
import logging
import pymongo
import sys
import getopt

class Scraper:
    def __init__(self, api, collection):
        self.api = api
        self.collection = collection
        self.logger = logging.getLogger('scraper.Scraper')

    def get_tweets_from_one_account(self, screen_name, since_id=None, inform=False):
        sleep_period = 60 * 15 + 1
        wait_period = 2
        
        while True:
            try:
                # Result set
                rs = self.api.user_timeline(screen_name=screen_name, count=200, since_id=since_id)
                wait_period = 2 # Reset wait period
                
                # Insert results into the mongodb collection
                tweets = [status._json for status in rs]
                self.collection.insert(tweets)

                if inform == True:
                    print "%s done" % (screen_name)
                return None

            except pymongo.errors.InvalidOperation, e:
                print "status list empty", e
                self.logger.warning("%s does not have any tweets", screen_name)
                self.logger.exception(e)
                return None

            except tweepy.error.TweepError, e:  # tweepy.error.TweepError
                if e.response.status_code == 404:
                    print e
                    print "%s does not exist" % (screen_name)
                    self.logger.warning('%s does not exist', screen_name)
                    self.logger.exception(e)
                    return None

                elif e.response.status_code == 429:
                    print e
                    # print "%s, sleeping until reset time, %s" % (datetime.datetime.now(), time.ctime(rate_limit_status['reset']))
                    print "%s sleep for 16 minutes" % (datetime.datetime.now())
                    self.logger.warning('%s is having problem', screen_name)
                    self.logger.exception(e)

                    # Sleep until the reset time
                    # time.sleep(rate_limit_status['reset'] - int(time.time()) + 20)
                    time.sleep(60 * 15 + 1)

                    rate_limit_status = (self.api.rate_limit_status()['resources']['statuses']
                                           ['/statuses/user_timeline'])
                    print rate_limit_status
                    continue
                elif e.response.status_code == 503:
                    print e
                    print "%s sleeping for an increasing while" % datetime.datetime.now()
                    self.logger.warning('%s is having problem', screen_name)
                    self.logger.exception(e)
                    time.sleep(wait_period)
                    wait_period *= 1.5
                    continue
                else:
                    print "%s, %s" % (screen_name, e)
                    continue                    

    def get_tweets_from_many_accounts(self, screen_names, inform=False):
        i = 1
        for screen_name in screen_names:
            self.get_tweets_from_one_account(screen_name, inform=inform)
            
            if inform == True:
                print "%s done" % (i)
            i += 1

        return None

    def get_friends_from_one_account(self, screen_name, inform=False):
        while True:
            try:
                ids = []
                for page in tweepy.Cursor(self.api.friends_ids, screen_name=screen_name).pages():
                    ids.extend(page)
                # Must use requests 2.7 See https://github.com/tweepy/tweepy/issues/659
                screen_names = [user.screen_name for user in self.api.lookup_users(user_ids=ids)]

                record = {"screen_name": screen_name,
                          "friends_ids": ids,
                          "friends_screen_names": screen_names}
                self.collection.insert_one(record)
                return None

            except tweepy.error.TweepError, e:  # tweepy.error.TweepError
                print screen_name
                print e
                if e.response.status_code == 404:
                    print e
                    print "%s does not exist" % (screen_name)
                    return None

                elif e.response.status_code == 429:
                    print e
                    print "%s sleep for 16 minutes" % (datetime.datetime.now())
                    time.sleep(60 * 15 + 1)
                    continue

    def get_friends_from_many_accounts(self, screen_names, inform=False):
        i = 1
        for screen_name in screen_names:
            self.get_friends_from_one_account(screen_name, inform=inform)
            if inform == True:
                print "%s done" % (i)
            i += 1
        return None

def main():
    # Change directory into the script's directory
    abspath = os.path.abspath(__file__) # path to file
    dname = os.path.dirname(abspath) # path to directory
    os.chdir(dname) # change to that directory

    # Start and format logger
    logger = logging.getLogger('scraper')
    fh = logging.FileHandler('twitter.log')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Get list of all congressmen' twitter id
    screen_names = pd.read_csv('data/twitter_id_list.csv').twitter_screen_name
    screen_names = screen_names[pd.notnull(screen_names)]

    # Connect to mongodb
    client = pymongo.MongoClient()
    db = client['politweets']

    # Authorizing Twitter API
    from passwords import consumer_key, consumer_secret, \
        access_token, access_token_secret
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    # Parse command line options
    options, remainder = getopt.getopt(sys.argv[1:], '', ['task='])
    for opt, arg in options:
        if opt == '--task' and arg == 'tweets':
            # Initialize and run the scraper for tweets
            collection = db['tweets']
            scraper = Scraper(api, collection)
            scraper.get_tweets_from_many_accounts(screen_names, inform=True)

        elif opt == '--task' and arg == 'friends':
            # Initialize and run the scraper for friends
            collection = db['friends']
            scraper = Scraper(api, collection)
            scraper.get_friends_from_many_accounts(screen_names, inform=True)
        else:
            print "No options like that"

if __name__ == '__main__':
    main()