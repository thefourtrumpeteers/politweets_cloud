import tweepy
import pandas as pd
import time, datetime
import os
import logging

def get_twitter_from_one_legislator(api, twitter_id, since_id=None):
    # Create logger
    logger = logging.getLogger('scraper')
    fh = logging.FileHandler('twitter.log')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    if pd.isnull(twitter_id):
        return None

    sleep_period = 60 * 15 + 1
    wait_period = 2
    
    while True:
        try:
            # Result set
            rs = api.user_timeline(screen_name=twitter_id, count=200, since_id=since_id)

            wait_period = 2 # Reset wait period
            return(
                pd.DataFrame({'id': [status.id_str for status in rs],
                              'created_at': [status.created_at for status in rs],
                              'text': [status.text for status in rs],
                              'retweet_count': [status.retweet_count for status in rs],
                              'favorite_count': [status.favorite_count for status in rs]})
            )

        except tweepy.error.TweepError, e:  # tweepy.error.TweepError
            if e.response.status_code == 404:
                print e
                print "%s does not exist" % (twitter_id)
                logger.warning('%s does not exist', twitter_id)
                logger.exception(e)
                return None

            elif e.response.status_code == 429:
                print e
                # print "%s, sleeping until reset time, %s" % (datetime.datetime.now(), time.ctime(rate_limit_status['reset']))
                print "%s sleep for 16 minutes" % (datetime.datetime.now())
                logger.warning('%s is having problem', twitter_id)
                logger.exception(e)

                # Sleep until the reset time
                # time.sleep(rate_limit_status['reset'] - int(time.time()) + 20)
                time.sleep(60 * 15 + 1)

                rate_limit_status = (api.rate_limit_status()['resources']['statuses']
                                       ['/statuses/user_timeline'])
                print rate_limit_status
                continue
            elif e.response.status_code == 503:
                print e
                print "%s sleeping for an increasing while" % datetime.datetime.now()
                logger.warning('%s is having problem', twitter_id)
                logger.exception(e)
                time.sleep(wait_period)
                wait_period *= 1.5
                continue
            else:
                print "%s, %s" % (twitter_id, e)
                continue

def main():
    # Change directory into the script's directory
    abspath = os.path.abspath(__file__) # path to file
    dname = os.path.dirname(abspath) # path to directory
    os.chdir(dname) # change to that directory

    # Get list of all congressmen' twitter id
    twitter_list = pd.read_csv('twitter_id_list.csv')

    # Authorizing Twitter API
    from passwords import consumer_key, consumer_secret, \
        access_token, access_token_secret
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    # Iterate through the list of twitter id's
    i = 1
    for twitter_id in twitter_list.twitter_id[pd.notnull(twitter_list.twitter_id)]:

        result = get_twitter_from_one_legislator(api, twitter_id)

        # If the result is a valid pandas DataFrame (i.e. not blank, none)
        # then write to csv
        if isinstance(result, pd.DataFrame):
            filename = '-'.join([datetime.datetime.now().strftime('%m-%d-%H-%M'),
                                 twitter_id])
            path = 'result/' + filename + '.csv'
            if not os.path.exists('result/'):
                os.makedirs('result/')

            result.to_csv(path, index=False, encoding = 'utf-8')
        print "%s done, number %s" % (twitter_id, i)
        i += 1
            
    # Concatenate and deduplicate all csv in the result folder
    list_of_files = []
    for f in os.listdir('result'):
        filename = os.path.join('result', f)
        df = pd.read_csv(filename,index_col=None, header=0)
        list_of_files.append(df)
    res = pd.concat(list_of_files).drop_duplicates()
    
    # Write the concatenated csv into a single csv
    if not os.path.exists('result_clean/'):
        os.makedirs('result_clean/')

    clean_filename = 'result_clean/' + datetime.datetime.now().strftime('%m-%d-%H-%M') + '-tweets.csv'
    res.to_csv(clean_filename, index=False, encoding = 'utf-8')

if __name__ == '__main__':
    main()