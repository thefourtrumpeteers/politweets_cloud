Final project analyzing US politicians' tweets

# For Prof and TAs
The main folders of your interests are:

1. `R`: this contains all of our code.
    - `0x_filename.R`: cleaning code
    - `1x_filename.R`: analysis code
    - `2x_filename.R`: making plot/table code
    
2. `write_up`: the main file is `FourTrumpeteers_final_project.tex`. Each individual section is partitioned in to corresponding `.tex` files

# Code style guide
1. The RStudio project file (`.Rproj`) already set the working directory at `politweets_cloud/`
2. Thus, we can use relative path, e.g.:
    - `read.csv("data/raw_data.csv")`
    - `write.csv("data_clean/cleaned_up_raw_data.csv")`

# Project structure

1. `data`: raw data files from outside
2. `data_clean`: cleaned data, **use these** for analysis
3. `figure`
3. `write_up`: Latex files. Include images in Latex by `\includegraphics{figure/myfigure}`)
5. `result`: save big result object here. That way we can just load up the stored result and make plots instead of re-running analysis
3. `R`: R code
    - `0x_filename.R`: cleaning code (load from `data`, save into `data_clean`)
    - `1x_filename.R`: analysis code (load from `data_clean`, save into `result`)
    - `2x_filename.R`: making plot/table code (load from `result`, save into `figure`)