import requests
import yaml
import pandas as pd

# Twitter screen names
params = {'apikey': '7f1f742af06e441fbc6316b64472e7d8',
          'per_page': 'all'}
r = requests.get('https://congress.api.sunlightfoundation.com/legislators',
    params = params).json()

d = pd.DataFrame({'first_name':
                      [l.get('first_name', pd.np.nan) for l in r['results']],
                  'last_name':
                      [l.get('last_name', pd.np.nan) for l in r['results']],
                  'twitter_screen_name': 
                      [l.get('twitter_id', pd.np.nan) for l in r['results']],
                  })
                  
d.to_csv('data/twitter_id_list.csv', encoding='utf-8', index = False)

# Legislators data
legislator = requests.get("https://raw.githubusercontent.com/unitedstates/"
                    "congress-legislators/master/legislators-current.yaml")
data = yaml.load(legislator.text)


def extract_bio(person):
    d = dict()
    d.update(person['name'])
    d.update(person['terms'][-1])  # Get last term only
    d.update(person['bio'])
    return d

# Extract data from yaml to a data frame
l = [extract_bio(person) for person in data]
df = pd.DataFrame(l)

# Write to csv
df.to_csv('data/legislators.csv', index=False, encoding='utf-8')


# Committees data
page = requests.get("https://raw.githubusercontent.com/unitedstates/"
                    "congress-legislators/master/committees-current.yaml")
data = yaml.load(page.text)


def extract_committee(committee):
    committee2 = dict(committee)
    if 'subcommittees' in committee2.keys():
        del committee2['subcommittees']
    
    return committee2

l = [extract_committee(committee) for committee in data]
df = pd.DataFrame(l)
df.to_csv('data/committees.csv', index=False, encoding='utf-8')