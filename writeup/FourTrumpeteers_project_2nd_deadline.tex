\documentclass{article} % For LaTeX2e
\usepackage{nips15submit_e,times}
\usepackage{hyperref}
\usepackage{url}
%\documentstyle[nips14submit_09,times,art10]{article} % For LaTeX 2.09


\title{The Four Trumpeteers Project}


\author{
Anh Le \\
\And
Luis Guirola \\
\And
Justin Max \\
\And
Emma Zang
}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}

\maketitle

\begin{abstract}
As US politicians increasing use Twitter to connect with their constituents and voice their positions, we propose using their tweets and their following patterns to identify clusters of politicians, either political, geographical, or demographic. Furthermore, the tweets from hard-core Democrats and Republicans could also be used to form a measure of left-right leaning, against which we can identify the media slant of major news outlet. Using text and social media network, this project offers insight into where the dividing lines are in American politics.
\end{abstract}

\section{Introduction}

Since communication between individuals is a form of purposeful behavior, it is likely to contain a lot of information about them. This is especially true in communication by politicians. Indeed, it can be used for position-taking on particular issues, getting information from voters, or building a narrative of their campaign. etc.

Communication in social networks, such as Twitter, is not an exception. Who they choose to interact with, what topics they tweet about, the choice of words they make, are purposeful and informational. Moreover, these patterns are likely to vary over time in reaction to the news cycle. An example is the live tweeting of primary debates. Who is likely to support (or avoid supporting) whom? What topics are they likely to react to more intensely? These are questions that can be analyzed with Twitter data.

\section{Motivating Questions}

We have two motivating questions.

First, how to measure media bias? Having a substantial number of tweets about congressmen of whom we know their political affiliation, we can measure how a news article is closer to one side or the other. If this was the case, for example, if certain media systematically paid more attention to topics to which Republican congressmen do, they could be considered as leaning towards that side of the political spectrum. 

Second, what is the pattern of clustering among politicians? This rejoins a large literature in political science on the issue of polarization (for example, see Fiorina, Abrams 2010). Certain scholars have suggested that America politics has become more divisive, resulting in two camps that do not listen to each other, do not talk about the same argument, and use different styles, etc. We are interested in finding evidence of this divisiveness in the clustering of tweets and Twitter following pattern. 

\section{Proposed Methodology}

\subsection{Data Collection}

To collect background information of US congressmen, including their official Twitter accounts, we use the \href{https://sunlightlabs.github.io/congress/legislators.html}{Sunlight foundation API}. The data includes their Party labels, constituency, time in Congress, social media profile, etc.

With a list of congressmen' Twitter accounts, we then \verb|Tweepy|, a Python library to access Twitter API and collect their tweets as well as their account information (e.g. their followers). We have used the script to download some sample data into \verb|.csv| (uploaded along with the document). Information from a tweet includes: text, time created, number of favorites / retweets, who favorites / retweets, and many more. The tweets will then be cleaned to clean up short-links, hash tags, etc.

The challenge is to automate this process. The current solution is to host the script on a server (Google App Engine, Amazon EC2), which periodically accesses Twitter and deposits the tweets into a database. We are working on making the script resilient to Twitter access limit (180 queries per 15 minutes) and automating the transfer of data from the cloud database to an offline, tabular dataset for analysis.\footnote{A not-so-minor challenge is how to do all this without paying for a server bill.}

\subsection{Clustering and Predictive}


We propose to use three methods learned in the class in order to answer our questions: information retrieval, clustering, and logistic regression. 

First, information retrieval provides a nice starting point for text analysis. A tweet, a headline, and an article can all be viewed as a ``bag of words." Presumably we will want to use some sort of scaling mechanism such as IDF-weighting because certain words will not be informative for our research questions. Using the bag-of-words representation, we can calculate the distance between various documents, as well as independent queries of our choosing. This will allow us to gauge the similarity of various politicians, media outlets, and so forth. We will of course have some further decisions to make---for example, is one observation a single tweet or is it the collection of all tweets from a particular congressman?

Second, clustering methods can be used to group together tweets, articles, and news outlets. It will be interesting to see how the congressman cluster on Twitter. One could imagine clustering by party, but they could also cluster by region or gender if these factors are highly correlated with tweet topics. If the tweets cluster by political party, increasing the number of clusters in a $k$-means algorithm could be informative as well. For example if $k=3$, we might be able to classify left, right, and moderate politicians (and perhaps finer partitions). 

Third, logistic regression can be complementary to clustering, particularly for estimating media slant. The binary outcome would be Democrat or Republican. Covariates could include individual word frequencies, average length of words, number of words per sentence, and hopefully more as we examine previous literature on the subject. Ideally, if tweets cluster by party line, then the tweet data could make a good training set. We could then use these parameters to predict out of sample, i.e. fit the newspapers articles to the model estimated on the tweets. Given that the list of words will almost certainly be larger than the number of observations, this could be a nice opportunity to use LASSO. We will see during the next few classes if the Bayesian LASSO might be useful here as well.

\subsection{Network visualization}

With the Twitter network data, showing the who-follows-whom and who-retweets-whom among congressmen, we will 1) visualize the network with Gephi; 2) apply social network analysis to explain clustering patterns. 

The social network analysis starts with transforming tabular data into directed edges among nodes (congressmen). Then, we will construct measures of network dynamics. Two most important measures are \textit{centrality} and \textit{modularity}.

Among different centrality measures, we choose eigenvector centrality, which is suitable for our purpose of understanding how important a congressman is in the Twitter network. Compared to other centrality measures, eigenvector centrality considers not only the number of followers a given congressman has, but also how influential his followers are (i.e. how many people follow his followers). In this way, eigenvector centrality is very similar to PageRank.

Among different modularity measures, we will apply a modularity based community finding algorithm. This method is a type of clustering algorithm that is designed for network data. Specifically, it clusters Twitter accounts into different communities by grouping the congressmen by the density of their local network.

After the network analysis and visualization, we will have a better understanding on how congressmen are clustered with each other, and perhaps can discern from this clustering its geographical and demographic causes (i.e. are Southern Republicans / female politicians / coastal Democrats connected among themselves?). Other information we can gain are: who are the most influential figures, whether the network structures are heterogeneous or homogeneous among the local networks.  

\end{document}
